import { Device } from "../Device";

export class CommunicationInterface {
      device: Device;

      constructor(device: Device) {
            this.device = device;
      }
}