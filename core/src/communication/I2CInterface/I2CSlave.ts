import { Device } from "../../Device";
import { UUID } from "../../UUID";
import { I2CAddress } from "./I2CAddress";
import { I2CBus } from "./I2CBus";
import { I2CMaster } from "./I2CMaster";
import { CommunicationInterface } from "../CommunicationInterface";

export class I2CSlave extends CommunicationInterface {

      address: I2CAddress;
      bus: I2CBus;

      constructor(device: Device, address: I2CAddress, bus: I2CBus) {
            super(device);
            this.address = address;
            this.bus = bus;
      }

      get master() {
            let master = this.bus.masters.find((e) => { return e.device.available(); });
            if (master == undefined)
                  throw new Error("no master available");
            return master;
      }

      writeRegister(register: number, value: number): void {
            this.master.writeRegister(this.address, register, value);
      }

}
