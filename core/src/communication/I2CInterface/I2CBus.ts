import { Medium } from "../../Medium";
import { I2CSlave } from "./I2CSlave";
import { I2CMaster } from "./I2CMaster";

export class I2CBus extends Medium {

      members: (I2CSlave | I2CMaster)[] = [];

      get masters(): I2CMaster[] {
            let result: I2CMaster[] = [];
            for (let e of this.members)
                  if (e instanceof I2CMaster)
                        result.push(e);

            return result;
      }

      get slaves(): I2CSlave[] {
            let result: I2CSlave[] = [];
            for (let e of this.members)
                  if (e instanceof I2CSlave)
                        result.push(e);

            return result;
      }

      add(device: I2CMaster | I2CSlave): void {
            this.members.push(device);
      }

}