import { Device } from "../../Device";
import { I2CAddress } from "./I2CAddress";
import { I2CBus } from "./I2CBus";
import { CommunicationInterface } from "../CommunicationInterface";

export abstract class I2CMaster extends CommunicationInterface {

      bus: I2CBus;

      constructor(device: Device, bus: I2CBus) {
            super(device);
            this.bus = bus;
      }

      abstract writeRegister(slave: I2CAddress, register: number, value: number): void;
      abstract readRegister(slave: I2CAddress, register: number): void;

      //TODO read byte, write bytes and so on
}