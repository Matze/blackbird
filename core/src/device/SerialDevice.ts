import * as SerialPort from "serialport"
import { SerialConnection } from "../connection/SerialConnection";

export class SerialDevice {

      serialPortIdentifier: string;
      _serialConnection: SerialConnection | null = null;

      constructor(serialPort: string) {
            this.serialPortIdentifier = serialPort;
      }

      get serialConnection(): SerialConnection {
            if (this._serialConnection == null) {
                  this._serialConnection = new SerialConnection(this.serialPortIdentifier);
            }

            return this._serialConnection;
      }

}