import { Device } from "../Device";
import { I2CAddress } from "../communication/I2CInterface/I2CAddress";
import { applyMixins } from "../util";
import { I2CBus } from "../communication/I2CInterface/I2CBus";
import { I2CSlave } from "../communication/I2CInterface/I2CSlave";

export class MCP23017 extends Device {

      i2c: I2CSlave;

      constructor(i2c: I2CSlave) {
            super();

            this.i2c = i2c;
      }

      // writeRegister(register: number, value: number) { }

      writeGPIO(pin: number, value: boolean) {
            // i2c.write register...
      }

}

// applyMixins(MCP23017, [I2CSlave]);