
import * as uuidv4 from 'uuid';

/**
 * Wrapper class for UUID.
 * Adds some handy functions.
 */
export class UUID {

      uuid: string;

      constructor() {
            this.uuid = uuidv4.v4();
      }

      get byteEncoding(): Uint8Array {
            let result = new Uint8Array();
            for (let i = 0; i < this.uuid.length; i++)
                  result.fill(this.uuid.charCodeAt(i));
            return result;
      }

}