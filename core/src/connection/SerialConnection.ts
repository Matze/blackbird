import { Connection } from "./Connection";
import * as SerialPort from "serialport"

export class SerialConnection extends Connection {

      port: SerialPort;

      constructor(path: string) {
            super();

            this.port = new SerialPort(path,
                  {
                        baudRate: 9600,
                        dataBits: 8,
                        stopBits: 1,
                        parity: 'none',
                  },
                  () => {
                        console.log("OPENED");
                  }
            );
      }

      send() {

            // start, cmd, flag, group, socket, esc, delimiter
            let sequence = [0xFF, 0xE4, 10, 0b10010000, 0b00010000, 0x00, 0xFF];

            this.port.write(sequence);

            console.log("WELLL");

      }

}

