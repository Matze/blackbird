import * as uuid from 'uuid';

import { Device } from "./Device";
import { Medium } from "./Medium";
import { UUID } from './UUID';

namespace NetworkProtocol {

  /**
   * TODO decode constructor
   */
  class NetworkProtocolMessage {

    /**
     * UUID of the message.
     */
    readonly uuid: UUID;

    /**
     * See [[NetworkProtocol.Path]]
     */
    path: Path;
    payload: any;

    constructor() {

      this.uuid = new UUID;
      this.path = new Path;

    }

    public get byteEncoding(): Uint8Array {
      return new Uint8Array; //TODO
    }

    public get checksum(): string {
      return 'asdf'; // TODO
    }

  }

  /**
   * In the blackbird network layer there are no routers. 
   * Each network instance acts as a dumb routing node forwarding messages
   * according to the path provided by the message.
   */
  class Path {

    readonly devices: Device[] = [];
    readonly media: Medium[] = [];
    currentPosition: number = 0;

    public addHop(medium: Medium, device: Device): void {
      if (!this.source) {
        throw new Error("Source must be defined first");
      }


      this.devices.push(device);
      this.media.push(medium);
    }

    public get source(): Device {
      return this.devices[0];
    }

    public set source(source: Device) {
      if (this.source) {
        this.devices[0] = source;
      } else {
        this.devices.push(source);
      }
    }

    public get destination(): Device {
      return this.devices[this.devices.length - 1];
    }

  }

}
