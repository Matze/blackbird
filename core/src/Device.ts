import * as uuidv4 from 'uuid';
import { UUID } from './UUID';

export class Device {

      /**
       * TODO
       */
      name: string;

      superModule: Device | null = null;

      constructor(superModule?: Device, moduleName?: string) {
            if (superModule != undefined)
                  this.superModule = superModule;

            if (moduleName != undefined)
                  this.name = moduleName;
            else
                  this.name = this.constructor.name + new UUID;
      }

      get canonicalName(): string {
            return (this.superModule != null) ? this.superModule.canonicalName : ""
                  + '/' + this.name;
      }

      available(): boolean {
            return false;
      }

}
